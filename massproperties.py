#!/usr/bin/env python2
import pymassproperties
import sys
import argparse

parser = argparse.ArgumentParser(
  description = "Computes mass properties.",
  epilog = """The inertial tensor is reported relative to the world-aligned frame
  with origin at the COM.""")
parser.add_argument('mesh', type=str, help = "mesh filename")
args = parser.parse_args()

V, COM, I = pymassproperties.compute(args.mesh)
print "V   = ", V
print "COM = ", COM
print "I   = ", I[0]
print "      ", I[1]
print "      ", I[2]