#!/usr/bin/env python2

# If massproperties or gazebomassproperties are executed in the module
# directory then this file will be imported. Otherwise __init__.py
# will import * from this file.

import os
import ctypes

from os.path import dirname, join, splitext, realpath, isfile

__all__ = ['compute']

# initialize module
_thelib = realpath(join(dirname(__file__),"libtrimesh_inertia_lib.so"))
#print _thelib
_dll = ctypes.cdll.LoadLibrary(_thelib)
_fun = getattr(_dll, 'compute')


def compute(filename):
  if not isfile(filename):
    raise IOError("Mesh file %s does not exist" % filename)
  _, ext = splitext(filename)
  filename = ctypes.c_char_p(filename)
  ext      = ctypes.c_char_p(ext)
  volume = ctypes.c_double()
  com = (ctypes.c_double * 3)()
  inertia = (ctypes.c_double * 6)()
  _fun(filename, ext, ctypes.byref(volume), ctypes.byref(com), ctypes.byref(inertia))
  I = inertia
  I = [[I[0], I[1], I[2]],
       [I[1], I[3], I[4]],
       [I[2], I[4], I[5]]]
  return volume.value, list(com), I