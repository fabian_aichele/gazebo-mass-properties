Gazebo Mass Properties
----------------------

This is a wrapper around libvcg to compute volume, center of mass and inertia tensors of polyhedral meshes.

Moreover it can output in gazebo compatible format. For example:

```
$gazebomassproperties gazebo-mass-properties/test/meshes/foot_clostermann.dae

<inertial>
  <pose>1.94872e+00 -2.53236e-01 1.45836e-02 0 0 0</pose>
  <mass>1.14845e+00</mass>
  <inertia>
    <ixx>1.53928e-01</ixx>
    <iyy>1.93858e+00</iyy>
    <izz>1.81418e+00</izz>
    <ixy>-7.51682e-02</ixy>
    <ixz>-8.51739e-03</ixz>
    <iyz>-6.50762e-04</iyz>
  </inertia>
</inertial>
```
`gazebomassproperties` accepts scale and density parameters.


Example 2:
```
$python massproperties.py test/meshes/unitsphere_blender_triangles.dae

V   =  4.18456840515
COM =  [2.670350909284025e-07, 1.2895910685983836e-06, -1.4417921967435632e-08]
I   =  [1.672815358595783, -3.007681946321287e-07, -5.686133605631571e-09]
       [-3.007681946321287e-07, 1.6728153861609147, -2.45834325891596e-09]
       [-5.686133605631571e-09, -2.45834325891596e-09, 1.6724773621528664]
```


Dependencies
------------
* libvcg (http://vcg.isti.cnr.it/vcglib/)
* python 2.7


Install
-------

The cmake script will install a python package in local/lib/pythonX.Y/site-packages
under the prefix where a python executable was found, if one was found.

Then it will create symlinks in the bin directory to the scripts in the package directory.

Limitations
-----------
* Only DAE and OBJ format supported, although it is easy to extend to anything libvcg supports.
* vcglib loads dae incorrectly
* No unit tests for correct use of scaling and density parameters.
