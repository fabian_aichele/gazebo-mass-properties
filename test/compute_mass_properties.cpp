#include <string>
#include <stdexcept>

extern "C"
{

int compute(const char* filename, const char *file_type_by_ext, double *volume, double *com, double *inertia);

}


int main( int argc, char **argv )
{
  std::string filename = argv[1];
  std::string ext;
  try
  {
    ext = filename.substr(filename.size()-4);
  }
  catch(const std::out_of_range &e)
  {
    printf("No filename extension (filename too small). Aborting.\n");
    return 1;
  }


  double volume;
  double com[3];
  double inertia[6];

  int ret = compute(filename.c_str(), ext.c_str(), &volume, com, inertia);
  if (ret != 0)
    return ret;

  printf("Volume %.5e\n",volume);
  printf("CenterOfMass %.5e %.5e %.5e\n",com[0],com[1],com[2]);
  printf("ItRow1 %.5e %.5e %.5e\n",inertia[0],inertia[1],inertia[2]);
  printf("ItRow2 %.5e %.5e %.5e\n",inertia[1],inertia[3],inertia[4]);
  printf("ItRow3 %.5e %.5e %.5e\n",inertia[2],inertia[4],inertia[5]);
  return 0;
}