#!/usr/bin/env python2
import os
import sys
import glob
import copy
import unittest
from math import pi
from os.path import join, dirname

testdir = dirname(__file__)
meshdir = join(testdir, 'meshes')

# Add module root to the search path so that pymassproperties can be found.
sys.path.append(join(testdir,".."))

import pymassproperties

MP_UNIT_CUBE = [ 1.0,
               [0., 0., 0.],
               [[0.166667, 0, 0], [0, 0.166667, 0], [0., 0., 0.166667]] ]

M = 4./3.*pi
MP_UNIT_SPHERE = [ M,
                  [0., 0., 0.,],
                  [[2./5.*M, 0., 0.], [0., 2./5.*M, 0.], [ 0., 0., 2./5.*M]] ]

# calculated from foot.obj
MP_FOOT = [1.14845,
           [1.94872, -0.253236, 0.0145837],
           [
             [0.153928, -0.0751682, -0.0085174],
             [-0.0751682, 1.93858, -0.000650757],
             [-0.0085174, -0.000650757, 1.81418]
           ]
          ]


class TestComputationResultsOfMeshes(unittest.TestCase):
  def assertAlmostEqualMP(self, mp1, mp2, EPS):
    v1, com1, it1 = mp1
    v2, com2, it2 = mp2
    self.assertAlmostEqual(v1, v2, delta = EPS)
    for q1, q2 in zip(com1, com2):
      self.assertAlmostEqual(q1, q2, delta = EPS)
    for r1, r2 in zip(it1, it2):
      for e1, e2 in zip(r1, r2):
        self.assertAlmostEqual(e1, e2, delta = EPS)

  def test_meshes_unit_cubes(self):
    for mesh in glob.glob(join(meshdir, 'unitcube_*.dae')):
      mp = pymassproperties.compute(mesh)
      self.assertAlmostEqualMP(mp, MP_UNIT_CUBE, 1.e-6)

  def test_meshes_unit_spheres(self):
    # Discretization errors demand a much looser threshold.
    for mesh in glob.glob(join(meshdir, 'unitsphere_*.dae')):
      mp = pymassproperties.compute(mesh)
      self.assertAlmostEqualMP(mp, MP_UNIT_SPHERE, 1.e-2)

  def test_meshes_foot(self):
    for mesh in ['foot.obj', 'foot_c4d_export.dae', 'foot_clostermann.dae']:
      mp = pymassproperties.compute(join(meshdir, mesh))
      self.assertAlmostEqualMP(mp, MP_FOOT, 1.e-4)

  def test_off_center_mesh(self):
    mp = pymassproperties.compute(join(meshdir, 'off_center_test_mass.dae'))
    groundtruth = copy.deepcopy(MP_UNIT_CUBE)
    groundtruth[1] = [ 100., 0., 0. ] # Because the mesh is offset 100 units from world origin.
    self.assertAlmostEqualMP(mp, groundtruth, 1.e-6)


if __name__ == '__main__':
  unittest.main()