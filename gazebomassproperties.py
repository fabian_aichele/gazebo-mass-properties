#!/usr/bin/env python2
import pymassproperties
import sys
import os
import argparse


def compute_and_print(filename, density, scale):
  volume, COM, I = pymassproperties.compute(filename)

  mass = volume*density*scale*scale*scale
  I = map(lambda R: map(lambda x: x*density*scale*scale*scale*scale*scale, R), I)
  COM = map(lambda q: q*scale, COM)

  print '<inertial>'
  print '  <pose>%.5e %.5e %.5e 0 0 0</pose>' % tuple(COM)
  print '  <mass>%.5e</mass>' % mass
  print '  <inertia>'
  print '    <ixx>%.5e</ixx>' % I[0][0]
  print '    <iyy>%.5e</iyy>' % I[1][1]
  print '    <izz>%.5e</izz>' % I[2][2]
  print '    <ixy>%.5e</ixy>' % I[0][1]
  print '    <ixz>%.5e</ixz>' % I[0][2]
  print '    <iyz>%.5e</iyz>' % I[1][2]
  print '  </inertia>'
  print '</inertial>'


parser = argparse.ArgumentParser(
  description = "Computes mass properties and output in sdf format.",
  epilog = """The inertial tensor is reported relative to the world-aligned frame
  with origin at the COM.""")
parser.add_argument('-s', dest = 'scale', type = float, help = 'Length scale factor.', default = 1.)
parser.add_argument('-d', dest = 'density', type = float, help = 'Density used to compute mass from volume after scaling.', default = 1.)
parser.add_argument('mesh', type=str, help = "mesh filename")
args = parser.parse_args()

compute_and_print(args.mesh, args.density, args.scale)