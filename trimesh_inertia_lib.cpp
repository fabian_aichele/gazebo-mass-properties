#include<cstdio>
#include<GL/gl.h>
#include<vcg/complex/complex.h>
#include <wrap/io_trimesh/import.h>
#include <wrap/io_trimesh/import_dae.h>
#include <wrap/io_trimesh/import_obj.h>
#include<vcg/complex/algorithms/inertia.h>

// BUG: Collada import does not handle up_axis tag correctly (?).

class MyEdge;
class MyFace;
class MyVertex;
struct MyUsedTypes : public vcg::UsedTypes<	vcg::Use<MyVertex>   ::AsVertexType,
                                            vcg::Use<MyEdge>     ::AsEdgeType,
                                            vcg::Use<MyFace>     ::AsFaceType>{};

class MyVertex  : public vcg::Vertex<MyUsedTypes, vcg::vertex::Coord3f, vcg::vertex::Normal3f, vcg::vertex::BitFlags  >{};
class MyFace    : public vcg::Face< MyUsedTypes, vcg::face::FFAdj, vcg::face::Normal3f, vcg::face::VertexRef, vcg::face::BitFlags > {};
class MyEdge    : public vcg::Edge<MyUsedTypes>{};
class MyMesh    : public vcg::tri::TriMesh< std::vector<MyVertex>, std::vector<MyFace> , std::vector<MyEdge>  > {};

extern "C"
{

__attribute__ ((visibility ("default"))) int compute(const char* filename, const char *file_type_by_ext, double *volume, double *com, double *inertia)
{
  MyMesh mesh;
  if (strcmp(file_type_by_ext,".obj")==0)
  {
    vcg::tri::io::ImporterOBJ<MyMesh>::Info info;
    vcg::tri::io::ImporterOBJ<MyMesh>::Open(mesh, filename, info);
  }
  else if (strcmp(file_type_by_ext,".dae")==0)
  {
    vcg::tri::io::InfoDAE info;
    vcg::tri::io::ImporterDAE<MyMesh>::Open(mesh, filename, info);
  }
  else
  {
    printf("Unsupported extension %s. Aborting.\n", file_type_by_ext);
    return -1;
  }

  Eigen::Matrix3d J;

  vcg::tri::Inertia<MyMesh> Ib(mesh);
  vcg::Point3f cc = Ib.CenterOfMass();
  Ib.InertiaTensor(J);
  // J is returned relativ to the frame with origin at the COM, aligned with world axes.

  volume[0] = Ib.Mass();
  com[0]    = cc[0];
  com[1]    = cc[1];
  com[2]    = cc[2];
  inertia[0] = J(0,0);
  inertia[1] = J(0,1);
  inertia[2] = J(0,2);
  inertia[3] = J(1,1);
  inertia[4] = J(1,2);
  inertia[5] = J(2,2);
  return 0;
}

}

